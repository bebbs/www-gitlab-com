---
layout: markdown_page
title: "Business Development"
---
BDR Handbook   

## On this page
* [Customer FAQ's](#FAQ)  
* [Variable Compensation Guidelines](#compensation)
* [Vacation Incentives](#vacation)
* [Daily Prospecting Toolkit](#Toolkit)
* [Buyer Types](#Buyer Types)
* [Research Process 3X3](#Research Process)
* [Basic Principles of Prospecting](#Basic)
* [Initial Cadence Sample](#Sample)
* [Cold Call Framework](#Cold)
* [Email Prospecting](#Email)
* [Outbound Process](#Outbound)

## Customer FAQ's<a name="FAQ"></a>  

BDRs are frequently asked the following questions in some form or another:  

* I didn’t get a confirmation email, what should I do?  
* I need to change my password, or my password change didn’t work, what should I do?  
* I want to use [username] but it says it’s already taken, even though that user has never actually done anything on his/her account. Can you remove that user so I can get the username?
* I made a mistake when setting up my account and need to change it. What should I do?
* GitLab.com seems slow. Why is that? Is anything being done to address this?
* I'm migrating from GitHub, BitBucket, SVN, etc. Do you have any migration tools?
* I'm looking to integrate with a Kanban board or project management tool. What do you recommend? What integrates? Is GitLab's project management functionality any good? Do you have any resources that address project management within GitLab?
* I really need [specific feature, integration, etc.]. Is that planned?
* I don't think [Feature X] works like it should. I think it should work [this way]. Can you fix it?
* What tools do you integrate with? Do you integrate with [Tool X]?
* How do groups work within GitLab? How do permissions work within groups? How many users can be in a group?
* Do read-only users still count toward our total user count for EE? What about bots?
* What are the different levels of permission within a project? Can I limit access for certain projects to specific users?
* Can I allow specific people to access my private projects, while keeping the project private?
* Does GitLab have git LFS or git annex functionality?
* I’m working with PostgreSQL and/or PostGIS. Is this compatible with GitLab? How does GitLab work with these?
* How many repositories can I have on my GitLab account?
* Are you really free? Will you really be free forever?
* How much does it cost for XX enterprise licenses? Do you send an invoice or can I pay by credit card?
* Do you provide any volume discounts?
* Do you provide any reseller discounts?
* There are some EE features we would like to use, but not all of them - can we pay per feature and add them to CE?
* How is GitLab.com backed up and secured?

## Variable Compensation Guidelines<a name="compensation"></a>

Full-time BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on results. Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

Guidelines for bonuses:
* Team and individual objectives are based on GitLab's revenue targets and adjusted monthly.
* Bonuses are paid quarterly.
* Bonuses may be based on various objectives
    * E.g. a new BDR's first month's bonus is typically based on completing <a href="https://about.gitlab.com/handbook/general-onboarding/" target="_blank">onboarding</a>
    * Bonuses may be tied to sales target attainment, <a href="https://about.gitlab.com/handbook/marketing/#okrs" target="_blank">OKRs</a>, or other objectives.

## BDR Vacation Incentives<a name="vacation"></a>

Note: this is a new, experimental policy started in June 2016: changes and adjustments are expected

At GitLab, we have an <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">unlimited time off policy</a>. The business development team has additional incentives for taking time off:
1. The first 5 business days taken off by a BDR in a calendar year prorates their target by 100%, rounded up (i.e. reduces the BDR's target by the period's target divided by the number of business days in the period, times the number of days taken off).
2. The second 5 business days are prorated by 50%, rounded up if applicable
3. The third 5 business days are prorated by 25%, rounded up if applicable

E.g. A BDR with a target of 200 opportunities in a January with 20 business days can reduce that month's target to 190 by taking 1 day off, 150 by taking 5 days off, 125 by taking 10 days off, or 113 by taking 15 days off.

Rules and qualifications for BDR Vacation Incentives:

* BDRs must give notice 30 days ahead of intended time off to by the Team Lead and Senior Demand Generation Manager (and ensure they acknowledge receipt within 48 hours)
* BDRs must add intended time off to the availability calendar
* Prorated days can be deferred (i.e. take PTO without prorating the target), but are lost at the end of the year. Incidentally, the math usually works out in favor of not deferring.
* BDRs can take additional vacation days (see <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab PTO policy</a>), but time off beyond 15 days will not adjust targets.

With <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab's PTO policy</a>), there's no need to give notice or ask permission to take time off. Unfortunately, a quota carrying BDR can expect to lose some earning potential for taking PTO, which often leads to BDRs not taking vacation. Another problem with unlimited PTO is that management loses forecasting power when BDRs take unexpected PTO, making it tempting for leadership to discourage vacation. We hope that this additional vacation incentive for the team acts as a balancing incentive to both parties by (a) providing a prorated quota for BDRs who plan ahead and (b) helping leadership plan and forecast better without discouraging vacation.

## Daily Prospecting Toolkit<a name="FAQ"></a>

#### Learn
- Read industry trends and articles about your job
- Reach out to high performers on the sales and business development teams to learn or improve a skill
- Read the BDR handbook

#### Research
- Add at least 10 new contacts to salesforce everyday
- Learn something new about all of your targeted accounts
- Look for a deposit you can make in your prospecting emails/calls from an article or study online

#### Plan
- Plan your day out entirely 
- Have metric based goals for scheduled slot whether it is calling, emailing, mining, or reading
- Try not not multitask, become focused and separate activities to increase efficiency

#### Prospect
- Pick up the phone and dial! You are not overstepping anyone's boundaries by doing this
- Email all newly added prospects
- Work your desired cadence of calls/emails/voicemails
- Follow up with historical leads that pertain to your targeted list
- Send content to at least 10 prospects without asking for anything in return

#### Evaluate
- Look at your most productive day and duplicate it
- Analyze and benchmark your numbers
- Make sure Salesforce is up to date with any account/contact information

#### Additional Resources
- <a href="https://university.gitlab.com/" target="_blank">GitLab University</a>
- <a href="https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6" target="_blank">GitLab Market and Ecosystem</a>
- <a href="http://docs.gitlab.com/ee/" target="_blank">GitLab Documentation</a>
- <a href="https://about.gitlab.com/2016/07/22/building-our-web-app-on-gitlab-ci/" target="_blank">GitLab CI</a>
- <a href="https://about.gitlab.com/features/#compare" target="_blank">GitLab Editions</a>
- <a href="https://about.gitlab.com/comparison/" target="_blank">GitLab compared to other tools</a>

## Buyer Types<a name="Buyer Types"></a>

#### Technical
- (End User)
- Collaborate, features, data

#### Functional
- (Director)
- Making team projects more collaborative 

#### Economic
- (CXO)
- Hit Team Targets and ROI

Your messaging for each one will be different. An end user or lower level developer will not care much about the ROI on our product.

## Research Process<a name="Research Process"></a>

3 X 3 X 3

Three things in three minutes about the:
- Organization
- Industry
- Individual

#### Where to Start?

1. Salesforce
    - Lost deals, users and customers (CE and EE)
    - Contacts
        - What relevant titles should we be reaching out to
    - Activity History
        - Who in the org have we called before that we can follow up with or reference
    - Cross reference with version.gitlab.com to see their version and available other usage info

2. Linkedin
    - Summary/Bio - Anything mentioned about job responsibilities or skills
    - Previous work - Any Gitlab Customers that we can reference
    - Connections - Are they connected to anyone at GitLab for a possible introduction

3. Website
    - Mission Statement
    - Press Releases/Newsroom
    - (ctrl F) search for keywords

## Basic Principles of Prospecting<a name="Basic"></a>
- Call about them, not you
- Be confident and passionate
- Aim high and wide
- Multithreading
- Don’t sell into an interruption
- Focus on the objective
- Make it easy to say yes
- Objection handling
- Get something actionable
- Reschedules are part of it

#### Initial Prospecting Process/Call Cadence:<a name="Sample"></a>
- 8-10 Calls (4-5 voicemails - Every other call/1-2 per week)
- 5-6 Emails (Include 2 InMails/1-2 per week)

Step 1 - Call VM/Email
- Day or two break

Step 2 - Email follow-up/InMail
- Day or two break

Step 3 - Call 2x
- Morning (no voicemail)
- Afternoon (voicemail)
    - Day or two break

Step 4 - Email (Deposit)
- 3rd party resource
    - Day or two break

Step 5 - Call 2x
- Morning (voicemail)
- Afternoon (no voicemail)
    - Day or two break
    
Step 6 - InMail follow-up/Call 2x
- Day or two break

Step 7 - Call VM/Email "Why Not?"
- Day or two break

Step 8 - Call VM/Email "Too Busy or Not Interested?"

## Cold Call Framework<a name="Cold"></a>

#### Introduction
Hi. This is (Name) from GitLab

#### Ask for Time
“Do you have just a moment?”

#### Magic Question
“Have you heard of GitLab before?”

#### Initial Message with IBS
“GitLab helps (Title) in the (Industry) industry solve issues around (pain & need) or
“(Industry specific client) used GitLab to (Case study value proposition)

#### Confirm that they are the right person
“I understand you are in charge of application/program development, is that correct?

#### Schedule meeting
"Do you have 15 minutes on (specific day and time) to discuss your developer life cycle?"

#### Initial Benefit Statement example
- GitLab is an end-to-end developer lifecycle solution that allows developers to collaborate on code, project management, and to create a seamless workflow between teams across the entire org.

#### Anatomy of a Prospecting Call
- Introduction………………………...Approach/Into
- Initial Benefit Statement………….Brief/IBS “Why me?”
- Qualification………………………..Ask questions
- Value proposition………………….Dive into IBS? “Why now?”
- Education…………………………...Educate about GitLab
- Next steps…………………………..Commitment and/or next steps

## Email Prospecting<a name="Email"></a>

#### Emails
- Execs read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your request clear
- Make your emails viewable from a mobile device

#### Email Components
- Subject line
- Preview Pain
- Opening Stanza (Best Practice is something about them)
- Benefit & Value Proposition
- Create Urgency
- Definite Ask with available days and times

#### Basho Makeup
- Include “you” or “your” in the subject line
- Customer specific reference in subject line
- First paragraph relevant customer news
- Second paragraph is the “so what” and why they should take your call
- End with a meeting request

#### Subject: Your new project on Git

Larry,

You recently launched a new version of the webpage XYZ. Congrats! I am sure everyone at XYZ are loving the expertise you bring to the table. I read on your web page that it was developed using Git. 

Everyday here at GitLab we help senior developers like yourself to collaborate on code, and project management in the IT departments of the Fortune 1000, ultimately leading to an average savings of 30% when compared to other tools on the market.

What day do you have 15 minutes on your calendar to discuss?

Thanks,

## Outbound Process<a name="Outbound"></a>

Each rep will be assigned a list of 15-30 targeted accounts to work on a monthly or quarterly basis.

#### Criteria for those accounts still TBD
- Tier
- Industry
- Number of Employees
- Revenue
- Populated with contacts and activity
- Last activity date
- Lost opportunities
- Former or current customers

#### Help Marketing create campaigns focusing on:
- Accounts using specific competitors
- Up-to-date Titles
- Up-to-date company sizes
- Contract end dates

#### Rules of engagement
- Outbound to work off of contacts and leads within SFDC
- Outbound does not touch any historical lead that has activity on it within the last 45 days. We are on the same team as inbound, no bad blood, etc.

#### Formal Weekly 1:1

BDR and BDR Team Lead

- Mental check-in (winning and success)

- Discuss progress on targeted accounts

- Coaching - email strategy, campaigns, cadence, best practices

- Review goals at the account level and personal level

- Strategy for next week

- Upcoming events/campaigns that can be leveraged

- Personal goals and commitments 








